import React from 'react';
import {
	View,
	Text,
	Image,
	ScrollView,
	Button,
	Platform,
	Dimensions,
	StyleSheet,
	TouchableWithoutFeedback,
	AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Switch } from 'react-native-switch';

const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height

export default class DrawerStyle extends React.Component {
	navLink(nav,text,icon) {
		return(
			            <TouchableWithoutFeedback style={{height: 50}} onPress={() => this.props.navigation.navigate(nav)}>
                    	<Text style={styles.link}>
                    	<Icon name={icon} type="font-awesome"size={25}/>
                             {" "}  {text}</Text>
            			</TouchableWithoutFeedback>
            			)
	}

	navLogout(text,icon){
	 return(
	             <TouchableWithoutFeedback style={{height:50}} onPress={()=>this.logout()}>
	              <Text style={styles.link}>
	              <Icon name={icon} type="antdesign" size={25} />
	             {" "} {text}</Text>
	              </TouchableWithoutFeedback>
	)


	}

	logout = async () => {
          AsyncStorage.clear()
            this.props.navigation.navigate('Login');

            }

       render() {
		return(
			<View style={styles.container}>
				<ScrollView style={styles.scroller}>
					<View style={styles.topLinks}>
						<View style={styles.profile}>
							<View style={styles.imgView}>
								<Image style={styles.img} source={require('../img/food.jpg')} />
							</View>

							<View style={styles.profileText}>

<Icon name="arrow-left-circle" size={30} style={{textAlign:"right",color:'black' ,marginBottom:125, marginLeft:40}} onPress={()=>this.props.navigation.goBack()}/>

							</View>
						</View>
					</View >
					<View style={styles.bottomLinks}>
					<View style={{flexDirection:'row',borderBottomWidth:1,borderBottomColor:'black',height:50,paddingTop:10}}>
                  <Text style={{fontWeight:'bold',color:'black',fontSize:18,marginLeft:20}}>Availability</Text>
                   <Text style={{fontWeight:'bold',color:'black',fontSize:18,marginLeft:70,marginRight:25}}>ONLINE</Text>
                  <Switch
                  style={{marginLeft:40}}
                  value={false}
                  circleSize={25}
                    barHeight={25}
                  onValueChange={(val) => console.log(val)}
                  disabled={false}
                 activeText={'On'}
                  inActiveText={'Off'}
                 backgroundActive={'grey'}
                backgroundInactive={'gray'}
                 circleActiveColor={'#00b300'}
                 circleInActiveColor={'#ff2b00'}/>
                  </View>
						{this.navLink('Home', 'Home','home')}
						{this.navLink('Menu', 'Menu','wunderlist')}
						{this.navLink('Offers', 'Offers','alert-decagram')}
						{this.navLink('OrderHistory', 'Order History','history')}
					    {this.navLink('Delivery', 'Delivery Person','motorbike')}
					    {this.navLink('Support', 'Support','message-text')}
					    {this.navLogout('Logout','logout')}
					</View>
				</ScrollView>

			</View>
		)
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,

	},
	scroller: {
		flex: 1,
	},
	profile: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		paddingTop: 5,
		borderBottomWidth: 1,
		borderBottomColor: 'black',
	},
	profileText: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
	},
	name: {
		fontSize: 20,

		color: 'white',
		textAlign: 'left',
	},
	imgView: {
		flex:1,
		paddingLeft: 20,
		paddingRight: 20,
	},
	img: {
		height: 150,
		width: '240%',

	},
	topLinks:{

		height: 160,
		backgroundColor: 'white',
	},
	bottomLinks: {
		flex: 1,
		backgroundColor: 'white',
		paddingTop: 10,
		paddingBottom: 450,
        color:'black'
	},
	link: {
		flex: .9,
		fontSize: 20,
		padding: 10,
		paddingLeft: 15,
		margin: 5,
		color:'black',
		textAlign: 'left',
		borderBottomWidth:1,
		borderBottomColor:'#a9a9a9',
        width:'105%'
	},
	footer: {
		height: 50,
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: 'white',
		borderTopWidth: 1,
		borderTopColor: 'black'
	},
	version: {
		flex: 1,
		textAlign: 'right',
		marginRight: 20,
		color: 'gray'
	},
	description: {
		flex: 1,
		marginLeft: 20,
		fontSize: 16,
	}
})