import React, { Component } from "react";
import {
View,
Text,
StyleSheet,
TouchableOpacity,
Dimensions
} from "react-native";
import {Icon,Header} from 'react-native-elements';
import { createStackNavigator, createDrawerNavigator, createMaterialTopTabNavigator, createSwitchNavigator,createAppContainer } from 'react-navigation'
import RBSheet from 'react-native-raw-bottom-sheet'

import Home from '../screens/home'
import DrawerStyle from './drawerStyle'
import Login from '../screens/login'
import SignUp from '../screens/signup'
import New from '../screens/new'
import Processing from '../screens/processing'
import Delivered from '../screens/delivered'
import Menu from '../screens/menu'
import AllItems from '../screens/allitems'
import OutOf from '../screens/outof'
import Offers from '../screens/offers'
import OrderHistory from '../screens/order-history'
import Support from '../screens/support'
import Delivery from '../screens/delivery'
import Active from '../screens/active'
import OfferHistory from '../screens/offer-history'
import Search from '../screens/search'
import AuthLoading from '../screens/authloading'

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
	drawerWidth: WIDTH*0.83,
	contentComponent: ({ navigation }) => {
    return(<DrawerStyle navigation={navigation} />)
	}
}

const TNavigator1= createMaterialTopTabNavigator({
    New:{
       screen:New,
},
    Processing:{
       screen:Processing,
},
    Delivered:{
       screen:Delivered,
}
},
{
      tabBarPosition: 'top',
      swipeEnabled: false,
      animationEnabled: true,
      tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'black',
      style: {
        backgroundColor: '#daa520',
      },
      labelStyle: {
        textAlign: 'center',
      },
      indicatorStyle: {
        borderBottomColor: 'black',
        borderBottomWidth: 2,
},
},
}
);

const TNavigator2= createMaterialTopTabNavigator({
    All_Items:{
        screen:AllItems
},
    Out_of_stock:{
        screen:OutOf
},
},
{
       tabBarPosition: 'top',
       swipeEnabled: false,
       animationEnabled: true,
       tabBarOptions: {
       activeTintColor: 'black',
       inactiveTintColor: 'black',
       style: {
       backgroundColor: '#daa520',
},
       labelStyle: {
        textAlign: 'center',
},
        indicatorStyle: {
        borderBottomColor: 'black',
        borderBottomWidth: 2,
},
},
}
);

const TNavigator3= createMaterialTopTabNavigator({
     Active:{
         screen:Active
},
     History:{
         screen:OfferHistory
},
},
{
         tabBarPosition: 'top',
         swipeEnabled: false,
         animationEnabled: true,
         tabBarOptions: {
         activeTintColor: 'black',
         inactiveTintColor: 'black',
         style: {
          backgroundColor: '#daa520',
          },
          labelStyle: {
          textAlign: 'center',
},
         indicatorStyle: {
         borderBottomColor: 'black',
         borderBottomWidth: 2,
},
},
}
);
const SNavigator = createStackNavigator({
      Home:{
          screen: TNavigator1,
          navigationOptions:({ navigation }) => ({
          headerStyle: {
          elevation:0,
          ShadowOpacity:0,
          backgroundColor: '#daa520',
          },
          headerTintColor: 'black',
          title: 'Orders Today',
          headerLeft: (
          <View style={{ paddingHorizontal: 20 }}>
          <Icon name="menu" size={40} onPress={() => navigation.toggleDrawer()}/>
          </View>
),
          headerRight:(
          <View style={{ paddingHorizontal: 40 , paddingTop:8}}>
          <Icon name="search1" type='antdesign' size={30} onPress={() => {this.RBSheet.open()  }}/>
           <RBSheet
                                                    ref={ref => {
                                                    this.RBSheet = ref;
                                                    }}
                                                    height={680}
                                                    duration={700}

                                                    >
                                                    <Header

                                                    leftComponent={<Icon name='arrowleft' type="antdesign" size={30} iconStyle={{marginBottom:30,color:'black'}} onPress={() => {this.RBSheet.close(); }}/>}
                                                    centerComponent={{ text:'Search Lorem Ipsum', style: { color: 'black',fontSize:20,marginBottom:22,fontWeight:'bold'} }}
                                                    containerStyle={{
                                                    backgroundColor: '#ffc44d',
                                                    height:'8%'
                                                    }}>
                                                    </Header>
                                                    <Search />

                                                    </RBSheet>
          </View>
)
}),
},
       Menu:{
          screen: TNavigator2,
          navigationOptions:({ navigation }) => ({
          headerStyle: {
          elevation:0,
          ShadowOpacity:0,
          backgroundColor: '#daa520',
},
          headerTintColor: 'black',
          title: 'Manage Menu',

           headerLeft: (
           <View style={{ paddingHorizontal: 20 }}>
           <Icon name="menu" size={40} onPress={() => navigation.toggleDrawer()}/>
           </View>
),
          headerRight:(
          <View style={{ paddingHorizontal: 40 , paddingTop:8}}>
          <Icon name="search1" type='antdesign' size={30} onPress={() => {this.RBSheet.open()  }}/>
          <RBSheet
           ref={ref => {
           this.RBSheet = ref;
           }}
           height={680}
           duration={700}>
           <Header
           leftComponent={<Icon name='arrowleft' type="antdesign" size={30} iconStyle={{marginBottom:30,color:'black'}} onPress={() => {this.RBSheet.close(); }}/>}
           centerComponent={{ text:'Search Lorem Ipsum', style: { color: 'black',fontSize:20,marginBottom:22,fontWeight:'bold'} }}
           containerStyle={{
           backgroundColor: '#ffc44d',
           height:'8%'
           }}>
           </Header>
           <Search />
           </RBSheet>
           </View>
)
}),
},
       Offers:{
            screen: TNavigator3,
            navigationOptions:({ navigation }) => ({
            headerStyle: {
            elevation:0,
             ShadowOpacity:0,
             backgroundColor: '#daa520',
},
            headerTintColor: 'black',
            title: 'Manage Offers',
            headerLeft: (
            <View style={{ paddingHorizontal: 20 }}>
            <Icon name="menu" size={40} onPress={() => navigation.toggleDrawer()}/>
            </View>
 ),
            headerRight:(
            <View style={{ paddingHorizontal: 40 , paddingTop:8}}>
            <Icon name="search1" type='antdesign' size={30} onPress={() => {
                                                                     this.RBSheet.open();
                                                                     }}/>

            <RBSheet
            ref={ref => {
            this.RBSheet = ref;
            }}
            height={680}
            duration={700}

            >
            <Header

            leftComponent={<Icon name='arrowleft' type="antdesign" size={30} iconStyle={{marginBottom:30,color:'black'}} onPress={() => {this.RBSheet.close(); }}/>}
            centerComponent={{ text:'Search Lorem Ipsum', style: { color: 'black',fontSize:20,marginBottom:22,fontWeight:'bold'} }}
            containerStyle={{
            backgroundColor: '#ffc44d',
            height:'8%'
            }}>
            </Header>
            <Search />

            </RBSheet>


            </View>
)
}),
},
        OrderHistory:{
             screen:OrderHistory,
             navigationOptions:({ navigation }) => ({
             headerStyle: {
             elevation:0,
             ShadowOpacity:0,
             backgroundColor: '#daa520',
},
             headerTintColor: 'black',
             title: 'Orders History',
             headerLeft: (
             <View style={{ paddingHorizontal: 20 }}>
             <Icon name="menu" size={40} onPress={() => navigation.toggleDrawer()}/>
              </View>
 ),
              headerRight:(
              <View style={{ paddingHorizontal: 40 , paddingTop:8}}>
              <Icon name="search1" type='antdesign' size={30} onPress={() => navigation.toggleDrawer()}/>
              </View>
 )
 }),
 },
         Delivery:{
               screen:Delivery,
               navigationOptions:({ navigation }) => ({
               headerStyle: {
               elevation:0,
               ShadowOpacity:0,
               backgroundColor: '#daa520',
},
               headerTintColor: 'black',
               title: 'Delivery Partners',
               headerLeft: (
               <View style={{ paddingHorizontal: 20 }}>
               <Icon name="menu" size={40} onPress={() => navigation.toggleDrawer()}/>
               </View>
),
               headerRight:(
               <View style={{ paddingHorizontal: 40 , paddingTop:8}}>
               <Icon name="search1" type='antdesign' size={30} onPress={() => {this.RBSheet.open()  }}/>
                <RBSheet
                                          ref={ref => {
                                          this.RBSheet = ref;
                                          }}
                                          height={680}
                                          duration={700}

                                          >
                                          <Header

                                          leftComponent={<Icon name='arrowleft' type="antdesign" size={30} iconStyle={{marginBottom:30,color:'black'}} onPress={() => {this.RBSheet.close(); }}/>}
                                          centerComponent={{ text:'Search Lorem Ipsum', style: { color: 'black',fontSize:20,marginBottom:22,fontWeight:'bold'} }}
                                          containerStyle={{
                                          backgroundColor: '#ffc44d',
                                          height:'8%'
                                          }}>
                                          </Header>
                                          <Search />

                                          </RBSheet>
               </View>
)
}),
},
         Support:{
               screen:Support,
               navigationOptions:({ navigation }) => ({
               headerStyle: {
               elevation:0,
               ShadowOpacity:0,
               backgroundColor: '#daa520',
},
               headerTintColor: 'black',
               title: 'Support Request',
               headerLeft: (
               <View style={{ paddingHorizontal: 20 }}>
                <Icon name="menu" size={40} onPress={() => navigation.toggleDrawer()}/>
                </View>
),
               headerRight:(
               <View style={{ paddingHorizontal: 40 , paddingTop:8}}>
               <Icon name="search1" type='antdesign' size={30} onPress={() => {this.RBSheet.open()  }}/>

               <RBSheet
                           ref={ref => {
                           this.RBSheet = ref;
                           }}
                           height={680}
                           duration={700}

                           >
                           <Header

                           leftComponent={<Icon name='arrowleft' type="antdesign" size={30} iconStyle={{marginBottom:30,color:'black'}} onPress={() => {this.RBSheet.close(); }}/>}
                           centerComponent={{ text:'Search Lorem Ipsum', style: { color: 'black',fontSize:20,marginBottom:22,fontWeight:'bold'} }}
                           containerStyle={{
                           backgroundColor: '#ffc44d',
                           height:'8%'
                           }}>
                           </Header>
                           <Search />

                           </RBSheet>
               </View>
)
}),
},




});

const  DNavigator= createDrawerNavigator({
               Home:SNavigator,
               Menu:SNavigator,
               Offers:SNavigator,

               Delivery:SNavigator,
               Support:SNavigator,

},
     DrawerConfig
);

const Switch=createSwitchNavigator({
                AuthLoading: AuthLoading,
                Login:Login,
                SignUp:SignUp,
                Home:DNavigator,

})
export default createAppContainer(Switch);

const styles = StyleSheet.create({
container: {
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center'
}
});