import React, { Component } from 'react';
import { AppRegistry, ListView, Text, View, StyleSheet, TouchableHighlight,FlatList,ScrollView,RefreshControl } from 'react-native';
import { SwipeListView,SwipeRow } from 'react-native-swipe-list-view';
import {Icon} from 'react-native-elements';


export default class OutOf extends Component {
constructor(){
super()
this.state = {
data: [],
refreshing: false,
};
}

componentDidMount() {
this.fetchData();
}

_onRefresh = () => {
this.setState({refreshing: true},this.fetchData);
};




fetchData = async () => {
try{
const response = await fetch("http://192.168.0.104/restaurant/api/menu/1",{
headers: {
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
}});
const json = await response.json();
this.setState({ data: json.menu,refreshing:false});

} catch (err) {
console.warn(err);
}
};


renderEntries({ item, index }) {
if(item.status=='Inactive'){


return(

<View key={item.name}>
            <SwipeRow
             rightOpenValue={-100}>
             <View style={{backgroundColor:'red',borderWidth:1,height:'150%'}}>
             <Text  style={{color:'white',textAlign:'right'}}>OUT OF STOCK</Text>
             </View>
             <View style={{backgroundColor:'white',borderBottomWidth:2,flexDirection:'row',justifyContent:'space-between'}}>
             <Text style={{fontSize:20}} >{item.name}</Text>
             <Icon name='chevron-double-left' type='material-community'/>
             </View>
             </SwipeRow>
             </View>
);
}
}


render() {
return (


<View style={styles.container}>
<ScrollView
refreshControl={
<RefreshControl
refreshing={this.state.refreshing}
onRefresh={this._onRefresh}
title='Pull to refresh'
/>
}
>

<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>
</ScrollView>
</View>

);

}
}

var styles = StyleSheet.create({
  container: {
    flex: 1,

  },

});