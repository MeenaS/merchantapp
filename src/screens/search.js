import React, { Component } from 'react';
import { AppRegistry, ListView, Text, View, StyleSheet, TouchableWithoutFeedback,TouchableOpacity,ScrollView } from 'react-native';
import { SearchBar } from 'react-native-elements';

class Search extends Component{
 state = {
    search: '',
  };

   updateSearch = search => {
      this.setState({ search });
    };
render(){
const { search } = this.state;
return(
<SearchBar
        placeholder="Search for Lorem ipsum"
        inputContainerStyle={{backgroundColor:'white'}}
        containerStyle={{backgroundColor:'white'}}
        onChangeText={this.updateSearch}
        value={search}
      />
);
}
}

export default Search;