import React, { Component } from "react";
import {
    View,
    Image,
    Text,
    StyleSheet,
    TextInput,
    KeyboardAvoidingView,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Alert
} from "react-native";
import { SocialIcon, Button, Icon} from 'react-native-elements';
import { LoginButton, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import ElevatedView from 'react-native-elevated-view'
import Toast, {DURATION} from 'react-native-easy-toast'
import { GoogleSignin, GoogleSigninButton,statusCodes, } from 'react-native-google-signin';


class Login extends Component {
constructor(){
super()
this.state = {
  username:'',
  password:'',
 user_name: '',
       avatar_url: '',
       avatar_show: false,
        userInfo: ''
};
}

 componentDidMount() {
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId:
        '1040608283557-gvatsblon5c63thfu75qtorqkq0mkbb2.apps.googleusercontent.com',
    });
  }
  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
    this.props.navigation.navigate('Home');
  };
  _getCurrentUser = async () => {
    //May be called eg. in the componentDidMount of your main component.
    //This method returns the current user
    //if they already signed in and null otherwise.
    try {
      const userInfo = await GoogleSignin.signInSilently();
      this.setState({ userInfo });
    } catch (error) {
      console.error(error);
    }
  };
  _signOut = async () => {
    //Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ user: null }); // Remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };
  _revokeAccess = async () => {
    //Remove your application from the user authorized applications.
    try {
      await GoogleSignin.revokeAccess();
      console.log('deleted');
    } catch (error) {
      console.error(error);
    }
  };

get_Response_Info = (error, result) => {
    if (error) {
      Alert.alert('Error fetching data: ' + error.toString());
    } else {
        this.AsignIn()
      //this.setState({ user_name: 'Welcome' + ' ' + result.name });
      //this.setState({ avatar_url: result.picture.data.url });
     // this.setState({ avatar_show: true })
    // this.props.navigation.navigate('Home');
    //console.error(result);
    }
  }

     onLogout = () => {

      this.setState({ user_name: null, avatar_url: null, avatar_show: false });

    }

name(text){
  this.setState({
  username:text

  })
  }

  password(text){
    this.setState({
    password:text

    })
    }

    login()
    {

    let col={}
    col.username=this.state.username,
    col.password=this.state.password
    var url = 'http://192.168.0.104/restaurant/api/check_login';
    fetch(url,{
    method:'POST',
    body:JSON.stringify(col),
    headers:new Headers({
    'Content-Type':'application/json'
    })

    }).then((response) => response.json())
                        .then((resJson) => {
                           if(resJson.res==1){
                           this.AsignIn()

                           }
                           else if(resJson.res==0){
                           Alert.alert('Invalid Email or Password');
                            }
                        })

    }

    AsignIn = async () => {
            await AsyncStorage.setItem('userToken','bitemii');
              Alert.alert('You are LoggedIn Successfully');
              this.props.navigation.navigate('Home');
        }

render() {
        return (

<View style={styles.container}>
<View style={{flex:.8,flexDirection:"column",justifyContent:"flex-start",alignItems:"flex-start"}}>
<Icon name="arrow-left" size={30} type='font-awesome' iconStyle={{margin:20,marginRight:30}} onPress={() => this.props.navigation.navigate('Home')}/>
<Text style={{fontSize: 20,fontWeight: 'bold',color: 'black',marginLeft:20}}>Login to your account{"\n"}to start ordering.</Text>
</View>


<View style={{flexDirection: 'row',justifyContent:'space-between'}}>

                         {this.state.avatar_url ?
                                  <Image
                                    source={{ uri: this.state.avatar_url }}
                                    style={styles.imageStyle} /> : null}

                                <Text style={styles.text}> {this.state.user_name}</Text>

                                <LoginButton
                                  readPermissions={['public_profile']}
                                  onLoginFinished={(error, result) => {
                                    if (error) {
                                      console.log(error.message);
                                      console.log('login has error: ' + result.error);
                                    } else if (result.isCancelled) {
                                      console.log('login is cancelled.');
                                    } else {
                                      AccessToken.getCurrentAccessToken().then(data => {
                                        console.log(data.accessToken.toString());
                                        const processRequest = new GraphRequest(
                                          '/me?fields=name,picture.type(large)',
                                        null,
                                        this.get_Response_Info
                                        );
                                        // Start the graph request.
                                        new GraphRequestManager().addRequest(processRequest).start();
                                      });
                                    }
                                  }}
                                   onLogoutFinished={this.onLogout}
                                />


                                <GoogleSigninButton
                                style={{ width: 200, height: 38 }}
                                size={GoogleSigninButton.Size.Wide}
                                color={GoogleSigninButton.Color.Light}
                                onPress={this._signIn}
                                />

</View>

                          <Text style={{textAlign:"center"}}>or</Text>

<ElevatedView elevation={7} style={styles.con}>

                           <TextInput style={styles.input}


                           onChangeText={(text)=>this.name(text,'username')}
                             placeholder = "10 digit mobile number"
                             autoCapitalize = "none"
                             />

                            <TextInput style={styles.input}
                            placeholder = "Password"
                             autoCapitalize = "none"

                              onChangeText={(text)=>this.password(text,'password')}
                              />

                             <Text style={{marginLeft:160,marginBottom:12}}>Forgot Password?</Text>

                             <Button
                             onPress={()=>this.login()}
                             buttonStyle={{marginRight:'10%',backgroundColor:"#daa520", width:120,marginTop:30}}
                             title="Login"
                             />

                           <TouchableOpacity onPress={()=>this.props.navigation.navigate('SignUp')}>
                             <Text style={{fontSize:15,fontWeight:'bold',marginTop:30,marginBottom:20}}>New User? Create an account</Text>
                            </TouchableOpacity>

</ElevatedView>
</View>

);
}
}

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,

         backgroundColor:"#daa520",
    },
     input: {
          margin: 20,
          height:'10%',
          width:'80%',
         borderBottomWidth:2,
             },
     con:{
           width:'90%',
           alignItems:"center",
           justifyContent:"center",
            borderRadius:20,
           backgroundColor: 'white',
           marginLeft:22,
           marginBottom:40,
           marginTop:10

               },

       text: {
          fontSize: 20,
          color: '#000',
          textAlign: 'center',

        },

        imageStyle: {

          width: 200,
          height: 300,
          resizeMode: 'contain'

        }
});

