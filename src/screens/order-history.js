import React, { Component } from 'react';
import { AppRegistry, ListView, Text, View, StyleSheet, TouchableWithoutFeedback,TouchableOpacity,FlatList,ScrollView,RefreshControl } from 'react-native';
import {Button,Header,Card,Icon} from 'react-native-elements'

import ElevatedView from 'react-native-elevated-view'

export default class OrderHistory extends Component {
constructor(){
super()
this.state = {
data: [],
refreshing: false,
};
}

componentDidMount() {
this.fetchData();
}

_onRefresh = () => {
this.setState({refreshing: true},this.fetchData);
};




fetchData = async () => {
try{
const response = await fetch("http://192.168.0.104/restaurant/api/order_history/1",{
headers: {
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
}});
const json = await response.json();
this.setState({ data: json.order_details,refreshing:false});

} catch (err) {
console.warn(err);
}
};


renderEntries({ item, index }) {

return(

<ElevatedView
          elevation={3}
          style={styles.stayElevated}>

<View  key={'mykey',index}>
<View style={{flexDirection:'row',justifyContent:'space-between',padding:10}}>
<View style={{flexDirection:'column',marginTop:10}}>
<Text style={{fontWeight:'bold'}}>{item.order_id}</Text>
<Text style={{fontWeight:'bold'}}>{item.customer_name}</Text>
<Text>{item.location}</Text>
</View>
<View style={{flexDirection:'column',marginTop:10}}>
<Text>{item.received}</Text>
<Text style={{color:'green',padding:8}}>{item.amount}</Text>
<Text style={{fontWeight:'bold'}}>{item.status}</Text>
</View>
</View>
</View>
</ElevatedView>
);

}


render() {
return (
<View style={styles.container}>
<ScrollView
refreshControl={
<RefreshControl
refreshing={this.state.refreshing}
onRefresh={this._onRefresh}
title='Pull to refresh'

/>
}
>
<View style={{flexDirection:"row",justifyContent:"space-between",backgroundColor:'#dcdcdc',height:40,padding:10}}>
<Text>Showing orders for</Text>
<Icon name="calendar" type="antdesign"  />
</View>
<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>

</ScrollView>
</View>


);

}
}

const styles = StyleSheet.create({
stayElevated: {
    width: 385,
    height: 100,
    margin: 10,
    backgroundColor: 'white'
  },

});