import React, { Component } from 'react';
import { AppRegistry, ListView, Text, View, StyleSheet, TouchableHighlight,FlatList,ScrollView,RefreshControl } from 'react-native';
import { SwipeListView,SwipeRow } from 'react-native-swipe-list-view';
import {Icon} from 'react-native-elements';

export default class AllItems extends Component {
constructor(){
super()
this.state = {
data: [],
refreshing: false,
};
}

componentDidMount() {
this.fetchData();

}

_onRefresh = () => {
this.setState({refreshing: true},this.fetchData);
};


fetchData = async () => {
try{
const response = await fetch("http://192.168.0.104/restaurant/api/menu/1",{
headers: {
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
}});
const json = await response.json();
this.setState({ data: json.menu,refreshing:false });
} catch (err) {
console.warn(err);
}
};


renderEntries({ item, index }) {

if(item.status=='Active'){

return(

<View key={item.name} >

<SwipeRow
leftOpenValue={100}

>
<View style={{backgroundColor:'green',borderWidth:1,height:120}}>
<Text style={{color:'white',marginLeft:10,marginTop:15}}>IN STOCK</Text>

</View>
<View style={{backgroundColor:'white',borderBottomWidth:1,flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{fontSize:20,fontWeight:'bold',color:'black',marginTop:20,marginLeft:15}} >{item.name}</Text>
<Icon name='chevron-double-right' type='material-community' iconStyle={{marginTop:10}} />
</View>
</SwipeRow>

</View>
)}
else{
return (


<View key={item.name} >
<SwipeRow
rightOpenValue={-100}

>
<View style={{backgroundColor:'red',borderWidth:1,height:120}}>
<Text style={{color:'white',textAlign:'right',marginTop:15}}>OUT OF STOCK</Text>

</View>
<View style={{backgroundColor:'white',borderBottomWidth:1,flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{fontSize:20,fontWeight:'bold',color:'black',marginTop:20,marginLeft:15}} >{item.name}</Text>
<Icon name='chevron-double-left' type='material-community' iconStyle={{marginTop:10}}/>
</View>
</SwipeRow>

</View>
);
}

}
render() {
return (
<View style={styles.container}>
<ScrollView
refreshControl={
<RefreshControl
refreshing={this.state.refreshing}
onRefresh={this._onRefresh}
title='Pull to refresh'

/>
}
>
<View style={{flexDirection:"row",justifyContent:"space-between",backgroundColor:'#dcdcdc',height:40,padding:10}}>
<Icon name="infocirlceo" type="antdesign" size={20}  />
<Text>Click on the arrows or Swipe items to left to mark as out of {'\n'} stock and swipe items to right to mark as in stock</Text>

</View>
<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>
</ScrollView>
</View>


);
}
}

var styles = StyleSheet.create({
container: {
flex: 1,
},
});