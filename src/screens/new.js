import React, { Component,PureComponent } from "react";
import { AppRegistry, ListView, Text, View, StyleSheet, TouchableWithoutFeedback,Alert, RefreshControl,ScrollView ,FlatList,AsyncStorage} from 'react-native';
import {Button,Icon} from 'react-native-elements'
import Toast, {DURATION} from 'react-native-easy-toast'
import RBSheet from 'react-native-raw-bottom-sheet'

export default class New extends Component {
constructor(){
super()
this.state = {
data: [],
interval:''
};
}

componentDidMount() {
this.fetchData();
this.interval=setInterval(this.fetchData, 1000);
}
componentWillUnmount(){

clearInterval(this.interval);

}

deleteItemById = id => {
const filteredData = this.state.data.filter(item => item.id !== id);
this.setState({ data: filteredData });
};

fetchData = async () => {
try{
const response = await fetch("192.168.0.104/restaurant_new/api/orders/71",{
headers: {
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
}});
const json = await response.json();
this.setState({ data: json.order_details });
} catch (err) {
console.warn(err);
}
};
accept_order(order_id)
{
var url1 = 'http://192.168.0.104/restaurant/api/accept_order/'+order_id;
fetch(url1,{
method:'GET',
}).then((response) => response.json())
.then((responseJson) => {
if(responseJson.status==1){
Alert.alert('Order Accepted')
}
})
}
reject_order(order_id)
{
var url1 = 'http://192.168.0.104/restaurant/api/reject_order/'+order_id;
fetch(url1,{
method:'GET',
}).then((response) => response.json())
.then((responseJson) => {
if(responseJson.status==1){
Alert.alert('Order Rejected')
}
})
}
ListEmpty = () => {
return (

<View style={styles.MainContainer}>
<Text style={{ textAlign: 'center' ,fontSize:30,fontWeight:"bold"}}>No Data Found</Text>
</View>
);
};
renderEntries({ item, index }) {

if(item.status==0){

return(

<View style={ styles.textContainer }>

<Text style={styles.title}>Order Id :{item.order_id}</Text>
<Text style={styles.title}>Received at :{item.received}</Text>
<View style={{flexDirection:'row', justifyContent: 'space-between',alignItems:'flex-end'}}>
<View style={{flexDirection:'column'}}>
<Text style={styles.title}>Name : {item.customer_name}</Text>
<Text style={styles.title}>{item.location}</Text>
</View>
<View style={{flexDirection:'row',marginLeft:150}}>
<Icon name='rupee' type='font-awesome' size={17} iconStyle={{color:'black',marginTop:5}}/>
<Text style={styles.title}>{item.amount}</Text>
</View>
</View>
<View style={{ flexDirection:'row' ,marginTop:10}}>
<TouchableWithoutFeedback
style={{padding: 10}}
onPress={()=>{this.accept_order(item.order_id),this.deleteItemById(item.id)}}>
<Text style={{marginTop:12,color:'green',fontWeight:'bold',fontSize:15}}>ACCEPT ORDER</Text>
</TouchableWithoutFeedback>
<Toast
ref="toast"
style={{backgroundColor:'black'}}
position='top'
positionValue={200}
fadeInDuration={2000}
fadeOutDuration={2000}
opacity={0.8}
textStyle={{color:'white'}}
/>


<TouchableWithoutFeedback style={{marginLeft:10}}
onPress={() => {this.reject_order(item.order_id)

}}
>


<Text style={{marginTop:12,color:'red',fontWeight:'bold',fontSize:15,marginLeft:180}}>DECLINE</Text>
</TouchableWithoutFeedback>


</View>
</View>
)}

}
render() {
return (

<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}



/>

);
}
}

var styles = StyleSheet.create({
container: {

backgroundColor: '#FFFFFF',
marginTop:5
},
textContainer: {

borderBottomWidth:1,
borderColor:'black',
paddingLeft:20,
width:'100%',
height:170,paddingTop:5
},
title: {
fontSize: 18,
fontWeight:'bold',
textAlign: 'left',

color:'black'
},
text: {
fontSize: 18,

textAlign: 'right',
color: '#333333',
marginBottom: 5,
},
MainContainer: {
justifyContent: 'center',
marginTop: 80,
},
});