import React, { Component } from 'react';
import { Platform,AppRegistry, ListView, Text, View, StyleSheet, TouchableWithoutFeedback,ScrollView,RefreshControl,FlatList } from 'react-native';
import {Button,Header,Card} from 'react-native-elements'
import NewOffer from '../screens/newoffer'
import RBSheet from 'react-native-raw-bottom-sheet'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Active extends Component {
constructor(){
super()
this.state = {
data: [],
refreshing: false,
};
}

componentDidMount() {
this.fetchData();
}

_onRefresh = () => {
this.setState({refreshing: true},this.fetchData);
};
fetchData = async () => {
try{
const response = await fetch("http://192.168.0.104/restaurant/api/offers/1",{
headers: {
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
}});
const json = await response.json();
this.setState({ data: json.offers,refreshing:false});

} catch (err) {
console.warn(err);
}
};

getTextStyle(t) {
if(t=="Open") {
return 1;

} if(t=="Solved"){
return 2;


}
else
return 3;


}

renderEntries({ item, index }) {

return(

<View key={item.id}>
<Card>
<Text style={styles.text}>{item.promocode}</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',marginTop:60,borderTopWidth:1,borderColor:'gray',marginRight:20}}>
<Text>Added on {item.from_date}</Text>
<Text>Expires on {item.to_date}</Text>
</View>
</Card>
</View>

);

}


render() {
return (
<View style={styles.container}>
<ScrollView
refreshControl={
<RefreshControl
refreshing={this.state.refreshing}
onRefresh={this._onRefresh}
title='Pull to refresh'

/>
}
>
<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>

</ScrollView>
<View style={{flex:1}}>
<Icon raised={true}
iconRight={true}
name='add-circle'
size={60}
style={{color:'#ffc44d', position: 'absolute', bottom:0, right:0,}}
onPress={() => {
this.RBSheet.open();
}}
/>

<RBSheet
ref={ref => {
this.RBSheet = ref;
}}
height={680}
duration={700}

>
<Header

leftComponent={<Icon name='close' size={30} style={{marginBottom:20,color:'black'}} onPress={() => {this.RBSheet.close(); }}/>}
centerComponent={{ text: 'Create New Offer', style: { color: 'black',fontSize:18,marginBottom:22,fontWeight:'bold',marginRight:40 } }}
containerStyle={{
backgroundColor: '#ffc44d',
height:'8%'
}}>
</Header>
<NewOffer />

</RBSheet>
</View>
</View>


);

}
}
var styles = StyleSheet.create({



text: {
fontSize: 22,
fontWeight:'bold',
textAlign: 'left',
color:'green'
}

});
