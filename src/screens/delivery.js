import React, { Component } from 'react';
import { AppRegistry, ListView, Text, View, ScrollView,StyleSheet,FlatList, TouchableWithoutFeedback,TouchableOpacity,Image,AsyncStorage,RefreshControl } from 'react-native';
import {Button,Header,Card} from 'react-native-elements'
import StarRating from 'react-native-star-rating';
import {Icon} from 'react-native-elements'
import ElevatedView from 'react-native-elevated-view'

export default class Delivery extends Component {
constructor(){
super()
this.state = {
data: [],
refreshing: false,
};
}

componentDidMount() {
this.fetchData();
}

_onRefresh = () => {
this.setState({refreshing: true},this.fetchData);
};

fetchData = async () => {
try{
const response = await fetch("http://192.168.0.104/restaurant/api/delivery_person/1",{
headers: {
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
}});
const json = await response.json();
this.setState({ data: json.delivery_person_details,refreshing:false});

} catch (err) {
console.warn(err);
}
};

renderEntries({ item, index }) {

return(

<ElevatedView
elevation={3}
style={styles.stayElevated}>

<View key={index} style={ styles.container }>
<View style={{flexDirection:'row',justifyContent:'space-between',padding:5}}>
<Image
style={{width:80, height: 80,marginTop:20,marginLeft:8}}
source={require("../img/delivery-boy.jpg")}
/>
<View style={{flexDirection:'column',marginTop:10}}>
<Text style={styles.title1}>{item.name}</Text>
<Text style={styles.title}>{item.mobile}</Text>
<Text style={{color:"black",fontWeight:'bold'}}> delivered {item.delivered_count} times</Text>
<View style={{flexDirection:"row"}}>
<Text style={{color:"black",fontWeight:'bold'}}>Overall rating </Text>
<StarRating
starSize={18}
disabled={false}
halfStarEnabled={true}
maxStars={5}
fullStarColor="#ffb82b"
rating={item.ratings}
selectedStar={(rating) => this.onStarRatingPress(rating)}
starStyle={{marginTop:3}}
/>
</View>
</View>

<Icon name='old-phone' type='entypo' iconStyle={{color:'green',marginTop:35}} />
</View>
</View>
</ElevatedView>

);

}


render() {
return (
<View style={styles.container}>
<ScrollView
refreshControl={
<RefreshControl
refreshing={this.state.refreshing}
onRefresh={this._onRefresh}
title='Pull to refresh'
/>
}
>
<View style={{backgroundColor:"#E8E8E8",height:40}}>
<Text style={{fontSize:14,fontWeight:"bold",color:'black',textAlign:"center",marginTop:10}}>Delivery partners who have delivered food for your customers.</Text>
 </View>
<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>
</ScrollView>
</View>

);

}
}

const styles = StyleSheet.create({
container: {
flex: 1,
backgroundColor: '#F8F8F8',
marginTop:1
},

title: {
fontSize: 16,
fontWeight:'bold',

marginRight:40,

color:'black'
},
title1: {
fontSize: 16,
fontWeight:'bold',

marginRight:40,

color:'black'
},
title2: {
fontSize: 16,
fontWeight:'bold',
textAlign:'left',


color:'red'
},
text: {
fontSize: 22,
fontWeight:'bold',
textAlign: 'left',

color:'green'
},
stayElevated: {
width: 390,
height: 120,
margin: 10,
backgroundColor: 'white',
borderRadius:10
},

});
