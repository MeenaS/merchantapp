import React, { Component } from "react";
import {
View,
Text,
StyleSheet,
TextInput,
TouchableOpacity,
TouchableWithoutFeedback
} from "react-native";
import RBSheet from 'react-native-raw-bottom-sheet'
import Toast, {DURATION} from 'react-native-easy-toast'
export default class Issue extends Component{
constructor()
{
super();
this.state={
question:'',
hotel_id:"1"
}
}
onClick(){

this.submit();
 this.refs.toast.show('Submit success',DURATION.LENGTH_LONG);
}
updateValue(text){
this.setState({
question:text
})
}

submit(){
let collection={}
collection.question=this.state.question,
collection.hotel_id=this.state.hotel_id
var url='http://192.168.0.104/restaurant/api/create_issue';
fetch(url,{
method:'POST',
body:JSON.stringify(collection),
headers:new Headers({
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
})

}).then(res=>res.json())
.catch(error=>console.error('Error:',error)).then(response=>console.log('success:',response));

}


render(){
return(
<View>
<Text style={{fontSize:18,fontWeight:'bold',color:'black',margin:20}}>What is your need?</Text>
<TextInput
multiline={true}
style={{borderWidth:2,borderColor:'#808080',backgroundColor:"#dcdcdc",height:80,marginLeft:30,textAlignVertical: "top",width:"80%"}}
placeholder="Please write and submit your request. Our support team will reach you shortly."
onChangeText={(text)=>this.updateValue(text,'question')}
/>


<TouchableOpacity onPress={()=>{this.onClick()}}>

<Text style={styles.submit}>SUBMIT REQUEST</Text>
 <Toast ref="toast"/>
</TouchableOpacity>
   <Toast
                    ref="toast"
                    style={{backgroundColor:'black'}}
                    position='top'
                    positionValue={200}
                    fadeInDuration={1000}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color:'white'}}
                />
</View>
);
}
}
const styles=StyleSheet.create({
submit:{
fontSize:16,
fontWeight:'bold',
padding:6,
color:'white',
textAlign:'center',
marginLeft:190,
marginTop:78,
borderWidth:2,
width:'40%',
height:'25%',
backgroundColor:"green",
borderColor:"green"
}



})