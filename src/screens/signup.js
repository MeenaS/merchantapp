import React, { Component } from "react";

import {
    View,
    Image,
    Text,
    StyleSheet,
    TextInput,
    KeyboardAvoidingView,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    Alert
} from "react-native";
import { SocialIcon, Button, Icon} from 'react-native-elements';
import ElevatedView from 'react-native-elevated-view'

class SignUp extends Component {
constructor(props){
super(props);
this.state={
name:'',
email:'',
mobile:'',
password:'',
response:'',
Error:''
}
}

name(text){
  this.setState({
  name:text

  })
  }

  email(text){
    this.setState({
    email:text

    })
    }
  mobile(text){
  this.setState({
  mobile:text

  })
  }

  password(text){
    this.setState({
    password:text

    })
    }

    signup()
    {

    let collection={}
    collection.name=this.state.name,
    collection.email=this.state.email,
    collection.mobile=this.state.mobile,
    collection.password=this.state.password
    if((collection.name || collection.email || collection.mobile || collection.password)==""){
    this.setState({Error:'please enter the details'});
    }
    var url = 'http://192.168.0.104/restaurant/api/create_hotel';
    fetch(url,{
    method:'POST',
    body:JSON.stringify(collection),
    headers:new Headers({
    'Content-Type':'application/json'
    })
    }).then((response) => response.json())
                    .then((responseJson) => {
                    if(responseJson.res==1){
                    Alert.alert('You created account Successfully');
                    this.props.navigation.navigate('Login');
                    }
                    else
                    Alert.alert('Email or Mobile already Exists');
                    })
    }

render() {
        return (

<View style={styles.container}>
<View style={{flex:.8,flexDirection:"column",justifyContent:"flex-start",alignItems:"flex-start"}}>
<Icon name="arrow-left" size={30} type='font-awesome' iconStyle={{margin:20,marginRight:30}} onPress={() => this.props.navigation.navigate('Login')}/>
</View>
<Text style={{fontSize: 20,fontWeight: 'bold',color: 'black',margin:16,marginRight:180,marginBottom:40}}>Get Started Now{"\n"}to Feast your hunger.</Text>

<View style={{flexDirection: 'row',alignItems:"center",justifyContent:"center"}}>

                         <SocialIcon
                           style={{width:130,height:40,marginRight:20}}
                           title='facebook'
                           button
                           type='facebook'
                         />

                          <SocialIcon
                          style={{backgroundColor:"red",width:130,height:40}}
                           title='google'
                           button
                           type='google'
                          />

</View>
                          <Text  style={{fontWeight: 'bold', marginTop:7,marginBottom:13,textAlign:"center"}}>or</Text>

<ScrollView>
<ElevatedView elevation={7} style={styles.con}>

                           <TextInput style={styles.input}
                             placeholder = "Full Name"
                             autoCapitalize = "none"

                              onChangeText={(text)=>this.name(text,'name')}
                             />

                            <TextInput style={styles.input}
                            placeholder = "Email"
                             autoCapitalize = "none"

                              onChangeText={(text)=>this.email(text,'email')}
                             />

                            <TextInput style={styles.input}
                            placeholder = "10 digit number"
                             autoCapitalize = "none"

                              onChangeText={(text)=>this.mobile(text,'mobile')}
                             />

                               <TextInput style={styles.input}
                               placeholder = "Password"
                               autoCapitalize = "none"

                                onChangeText={(text)=>this.password(text,'password')}
                              />
 <Text style={{color:"red", fontSize:18}}>{this.state.Error}</Text>
                             <Button
                            onPress={()=>this.signup()}
                             buttonStyle={{marginRight:20,backgroundColor:"#daa520", width:170,marginTop:30}}
                             title="CREATE ACCOUNT"
                             />



                           <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
                             <Text style={{fontSize:15,fontWeight:'bold',marginTop:30,marginRight:45,marginBottom:20}}>Existing User? Login</Text>
                            </TouchableOpacity>

</ElevatedView>
</ScrollView>
</View>

);
}
}

export default SignUp;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:"#daa520",
    },
     input: {
          margin: 10,
          height: 40,
          width:280,
         borderBottomWidth:2,
             },
      con:{
      borderRadius:20,
      width:'90%',
      alignItems:"center",
      justifyContent:"center",
      marginLeft:22

      }

});