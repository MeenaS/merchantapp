import React, { Component,PureComponent } from "react";
import { AppRegistry, ListView, Text, View, StyleSheet, TouchableWithoutFeedback,Alert, RefreshControl,ScrollView ,FlatList,AsyncStorage} from 'react-native';
import {Button,Icon} from 'react-native-elements'
import Toast, {DURATION} from 'react-native-easy-toast'
import RBSheet from 'react-native-raw-bottom-sheet'

export default class Processing extends Component {
constructor(){
super()
this.state = {
data: [],
interval:''
};
}

componentDidMount() {
this.fetchData();
this.interval=setInterval(this.fetchData, 1000);
}

componentWillUnmount(){
clearInterval(this.interval);
}

fetchData = async () => {
try{
const response = await fetch("http://192.168.0.104/restaurant/api/orders/1",{
headers: {
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
}});
const json = await response.json();
this.setState({ data: json.order_details });
} catch (err) {
console.error(err);
}
};


renderEntries({ item, index }) {

if(item.status=="Accepted"){

return(

<View key={item.name} style={ styles.textContainer }>
<View style={{flexDirection:"row"}}>
<Text style={styles.title}>Order Id :{item.order_id}</Text>
<Icon name='rupee' type='font-awesome' size={17} iconStyle={{color:'black',marginTop:5,marginLeft:200}}/>
<Text style={{color:"green",fontSize:18}}>{item.amount}</Text>
</View>
<Text style={styles.title}>Received at :{item.received}</Text>
<View style={{flexDirection:'row', justifyContent: 'space-between',alignItems:'flex-end'}}>
<View style={{flexDirection:'column'}}>
<Text style={styles.title}>Name : {item.customer_name}</Text>
<Text style={styles.title}>{item.location}</Text>
</View>
<Icon iconStyle={{position: "absolute", bottom: 0, left: 150}}name="crosshairs-gps" type="material-community" size={45} />

</View>

</View>
)}

}
render() {
return (

<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>

);
}
}

var styles = StyleSheet.create({
container: {
flex: 1,
backgroundColor: '#FFFFFF',
marginTop:5
},
textContainer: {
justifyContent:'flex-start',
alignItems:'flex-start',
borderBottomWidth:1,
borderColor:'black',
paddingLeft:20,
width:'100%',
height:170,paddingTop:5
},
title: {
fontSize: 18,
fontWeight:'bold',
textAlign: 'left',
},
text: {
fontSize: 18,

textAlign: 'right',
color: '#333333',
marginBottom: 5,
},
});