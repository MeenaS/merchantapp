import React, { Component,PureComponent } from "react";
import { AppRegistry, ListView, Text, View, StyleSheet, TouchableWithoutFeedback,Alert, RefreshControl,ScrollView ,FlatList,AsyncStorage} from 'react-native';
import {Button,Header,Card} from 'react-native-elements'
import Toast, {DURATION} from 'react-native-easy-toast'
import RBSheet from 'react-native-raw-bottom-sheet'
import Ticket from './ticket'
import Icon from 'react-native-vector-icons/MaterialIcons'


export default class Support extends Component {
constructor(){
super()
this.state = {
data: [],
refreshing: false,
};
}

componentDidMount() {
this.fetchData();
}
_onRefresh = () => {
this.setState({refreshing: true},this.fetchData);
};
fetchData = async () => {
try{
const response = await fetch("http://192.168.0.104/restaurant/api/list_issue/1",{
headers: {
'Accept': 'application/json, text/plain, */*', // It can be used to overcome cors errors
'Content-Type': 'application/json'
}});
const json = await response.json();
this.setState({ data: json.status,refreshing:false});
} catch (err) {
console.warn(err);
}
};
getTextStyle(t) {
if(t=="Open") {
return 1;
} if(t=="Solved"){
return 2;
}
else
return 3;
}
renderEntries({ item, index }) {
return(
<View style={{backgroundColor:'#dcdcdc'}}>
<Card>
<View style={{ flexDirection:'row'}}>
<Text style={styles.text}>#2245{item.id}</Text>
<Text style={[(this.getTextStyle(item.q_status)==1)? styles.textvalid1:styles.text2,
(this.getTextStyle(item.q_status)==2)? styles.textvalid2:styles.text2,
(this.getTextStyle(item.q_status)==3)? styles.textvalid3:styles.text2, ]}>{item.q_status}</Text>
</View>
<Text style={styles.title1}>Created on:{item.created_at}</Text>
<Text style={styles.title1}>Query:{item.question}</Text>
<Text style={styles.title1}>Response:Request received {item.question}{"\n"} our support team will contact you soon</Text>
</Card>
</View>
);
}
render() {
return (
<View style={styles.container}>
<ScrollView
refreshControl={
<RefreshControl
refreshing={this.state.refreshing}
onRefresh={this._onRefresh}
title='Pull to refresh'
/>
}
>
<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>
</ScrollView>
<View style={{flex:1}}>
<Icon raised={true}
iconRight={true}
name='add-circle'
size={60}
style={{color:'#ffc44d', position: 'absolute', bottom:10, right:10,}}
onPress={() => {
this.RBSheet.open();
}}
/>

<RBSheet
ref={ref => {
this.RBSheet = ref;
}}
height={680}
duration={100}

>
<Header

leftComponent={<Icon name='close' size={30} style={{marginBottom:20,color:'black'}} onPress={() => {this.RBSheet.close(); }}/>}
centerComponent={{ text: 'Create New Offer', style: { color: 'black',fontSize:18,marginBottom:22,fontWeight:'bold',marginRight:40 } }}
containerStyle={{
backgroundColor: '#ffc44d',
height:'10%'
}}>
</Header>
<Ticket />

</RBSheet>
</View>
</View>

);

}
}

var styles = StyleSheet.create({
container: {
flex: 1,
backgroundColor: '#FFFFFF',
marginTop:10
},
textContainer: {
justifyContent:'flex-start',
alignItems:'flex-start',
borderWidth:1,
borderColor:'black',
paddingLeft:10,
width:'95%',
height:130,paddingTop:5,
margin:10,
borderRadius:10
},
title: {
fontSize: 16,
fontWeight:'bold',
textAlign:'left',
marginRight:80,

color:'black'
},
title1: {
fontSize: 16,
fontWeight:'bold',
textAlign:'left',
marginRight:40,

color:'black'
},
title2: {
fontSize: 16,
fontWeight:'bold',
textAlign:'left',
marginRight:40,

color:'black'
},
text: {
fontSize: 22,
fontWeight:'bold',
textAlign: 'left',

color:'green'
},
text2: {
fontSize: 18,
fontWeight:'bold',
textAlign:'right',
marginLeft:200
},
textvalid1: {
color:'red'
},
textvalid2: {
color:'green'
},
textvalid3: {
color:'orange'
},

});

